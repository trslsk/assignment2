/*
*name: Tan Rou Shan
*A Member class that contain member object
*/
public class Member {
    
    //instance variable
    private String name;
    private double weightInKg;
    private double heightInM;
    int numOfAct;
    ActivityList mylist=new ActivityList(20);
    
    //no-argument constuctor
    public Member()
    {
        name="not set";
        weightInKg=0.0;
        heightInM=0.0;
    }
    
    //contructor with argument
    public Member(String inName, double inWeight, double inHeight)
    {
        if (!inName.equals(""))
            name=inName;
        else
            name="not set";
        if (inWeight>0)
            weightInKg=inWeight;
        else
            weightInKg=0.0;
        if (inHeight>0)
            heightInM=inHeight;
        else 
            heightInM=0.0;
    }
    
    //getters
    public String getName()
    {
        return name;
    }
    
    public double getWeight()
    {
        return weightInKg;
    }
    
    public double getHeight()
    {
        return heightInM;
    }
    
    //setters
    public void setName(String newName)
    {
        if(!newName.equals(""))
            name=newName;
    }
    
    public void setWeight(double newWeight)
    {
        if(newWeight>0)
           weightInKg=newWeight;
    }
    
    public void setHeight(double newHeight)
    {
        if (newHeight>0)
            heightInM=newHeight;
    }
    
    public String toString()
    {
        return name + ", your weight is "+ weightInKg +"kg and your height is "+ heightInM+ "m.";
    }
    
    public double getBMI()
    {
        return weightInKg/(heightInM*heightInM);
    }
    
    public boolean addRecord(Activity a)
    {
        if(numOfAct== mylist.SIZE)
            return false;
        mylist.activity[numOfAct++]= a;
              return true;   
    }
    
    public String showAct()
    {
      String str="\nThis is activities of "+ getName()+": ";
        for(int i=0; i< numOfAct;i++)
        {
            str+="\n"+(i+1)+". "+mylist.activity[i];
        }
	return str;
    }
      
    public double totalDuration(double duration)
    {
        double totalDuration=0;
        for(int i=0; i< numOfAct;i++)
        {
            totalDuration+=mylist.activity[i].getDurationInHours();
        }
        return totalDuration;
    }
      
    public double actTotalCost()
    {
        double totalCost=0;
        for(int i=0; i< numOfAct; i++)
        {
            totalCost+=mylist.activity[i].totalCost();
        }
        return totalCost;
    }
      
    public double calBurned()
    {
       double calBurned=0;
       for(int i=0; i< numOfAct; i++)
       {
           calBurned=mylist.activity[i].getMET()*getWeight()*
                   mylist.activity[i].getDurationInHours();
       }
         return calBurned; 
    }
      
    public double totalCalorieBurned()
    {
       double totalCalBurned=0;
       for(int i=0; i< numOfAct; i++)
       {
           totalCalBurned+=mylist.activity[i].getMET()*getWeight()*
                   mylist.activity[i].getDurationInHours();;
       }
       return totalCalBurned;
    }
      
    public String bodyType()
    {
     double bmi=getBMI();
        if(bmi>35)
            return "Morbidly obese";
        else if(bmi>=30)
            return "Obese";
        else if(bmi>=25)
            return "Overweight";
        else if (bmi>=18.5)
            return "Healthy";
        else 
            return "Underweight";
    }
    }
