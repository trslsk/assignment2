/**
 *name: Tan Rou Shan
 * A container class to store list of activities
 */
public class ActivityList {
    
    //instance variable
    final int SIZE=50;
    public Activity [] activity;//**
     int numOfActivity;
    
    //construtor with set to defaut size
    public ActivityList()
    {
        activity = new Activity [SIZE];
	numOfActivity = 0;
    }
    
    // constructor which sets to size passed in
    public ActivityList (int size)
    {
        if (size <0)
            activity = new Activity[SIZE];
        else
            activity = new Activity [size];
	numOfActivity = 0;
    }
    
    //a method to add an Activity 
    public boolean addActivity(Activity act)
    {
	if (numOfActivity == activity.length)
            return false;
	activity[numOfActivity++] = act;
            return true;
    }
	
    //a method to find an Activity given the name
    public Activity findActivity (String activityName)
    {
	for (int i = 0; i < numOfActivity; i++)
          if (activity[i].getActivityName().equalsIgnoreCase(activityName))
            return activity[i];
         return null; // not found
    }
	
    //a method to find the average MET of all activities
    public double averageMET()
    {
	double totalMET = 0.0;
            for (int i = 0; i < numOfActivity; i++)
            {
                totalMET+= activity[i].getMET();
            }
	return totalMET/numOfActivity;
    }
	
    //a method to find the activity with the highest total cost
    public Activity highestTotalCost()
    {
       Activity maxTotalCost = activity[0];
       for(int i =0; i< numOfActivity; i++)
           if(activity[i].totalCost() > maxTotalCost.totalCost())
           {
               maxTotalCost = activity[i];
               return activity[i];
           }
          return maxTotalCost;
    }
    //method to get all activities
    public String getAll() 
    {
	    String msg = "All activities\n";
	    for (int i = 0; i < numOfActivity; i++)
        { 
            msg+=(i+1) +". ";
            msg += activity[i].toString()+ "\n";
        }
	    return msg;
    }

}// end of Activity List