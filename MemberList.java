/*
Part 2 Member List
Name: Loh Suet Kee
Student ID: B 1501031
*/
package assign2;

public class MemberList {
   public class MemberList {
    //instance variable
   public final int SIZE = 50; 
   private Member[] account;
   private int numOfMember;
   
   //constructor to set default size
   public MemberList()
   {
       account= new Member[SIZE];
       numOfMember = 0;
       
   }
   
   //constructor which sets to size passed in
   public MemberList(int size)
   {
       if (SIZE<0)
           account = new Member[SIZE];
       else
           account = new Member[size];
       numOfMember = 0;
   }
   
    // method to add Member List
   public boolean addMember(Member newMb)
   {
       if(numOfMember == account.length)
           return false;
       account[numOfMember ++]= newMb;
       return true;
   }
   
    // method to find Member Name
   public Member findMember(String memberName)
   {
       for(int i =0; i<numOfMember; i++)
           if (account[i].getName().equalsIgnoreCase(memberName))
               return account[i];
       return null;
   }
   
   //method to find the Member with the highest BMI
   public Member findHighestBMI(double maxBMI)
   {
       maxBMI = account[0].getBMI();
       for(int i =0; i< numOfMember; i++)
           if(account[i].getBMI() > maxBMI)
               maxBMI = account[i].getBMI();
       return account[0];
   }
   
   //method to find the average weight of all members
   public double aveWeightMember()
   {
       double sum = 0.0;
       for(int i = 0; i < numOfMember; i++)
                sum += account[i].getWeight(); // total all the weights
        return sum / numOfMember;
   }
   
   //method to find average height
    public double aveHeightMember()
   {
       double sum = 0.0;
       for(int i = 0; i < numOfMember; i++)
                sum += account[i].getHeight(); // total all the weights
       return sum / numOfMember;
   }
      
    public String getAll() 
    {
	    String msg = "All Information of Members\n";
	    for (int i = 0; i < numOfMember; i++)
        {
            msg += account[i].toString()+ "\n";
        }
        return msg;
    }
}
