/*
name: Loh Suet Kee
An Activity class that contain activity object
*/
package assignment.pkg2;

//instance variable
public class Activity {
    private String activityName;
    private double met;
    private double durationInHours;
    private double costPerHour;
    
    //no arguments contructor
    public Activity()
    {
        activityName = "not set";
        met = 0.0;
        durationInHours = 0.0;
        costPerHour = 0.0;
    }
    
    //argument constructor
    public Activity(String inActivityName, double inMET, double inDurationInHours,
            double inCostPerHour )
    {
      if (inActivityName.equals(""))
          activityName = "not set";
      else
          activityName = inActivityName;
      
      if (inMET >0)
          met = inMET;
      else
          met = 0.0;
      
      if(inDurationInHours > 0)
          durationInHours = inDurationInHours;
      else
          durationInHours = 0.0;
      
      if(inCostPerHour > 0)
          costPerHour = inCostPerHour;
      else
          costPerHour = 0.0;
    }
    
    //getters
    public String getActivityName()
    {
        return activityName;
    }
    
    public double getMET()
    {
        return met;
    }
    
    public double getDurationInHours()
    {
        return durationInHours;
    }
    
    public double getCostPerHour()
    {
        return costPerHour;
    }
    //setters
    public void setActivityName(String newActivityName)
    {
        if(!newActivityName.equals(""))
            activityName = newActivityName;
    }
    
    public void setMET(double newMET)
    {
        if(newMET > 0)
            met = newMET;
    }
    
    public void setDurationInHours(double newDurationInHours)
    {
        if (newDurationInHours > 0)
            durationInHours = newDurationInHours;
    }
   
    public void setCosPerHours(double newCostPerHour)
    {
        if(newCostPerHour > 0)
            costPerHour = newCostPerHour;
    }
    
    //return toString from all instance variables
    public String toString()
    {
        return  activityName + " have MET value of " + met + ", with "
                + durationInHours +" hours and the cost per hour is RM"
                + costPerHour;
    }
    
    //total Cost Method
    public double totalCost()
    {
        return durationInHours * costPerHour;
    }
}