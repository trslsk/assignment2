/*
* name: tan rou shan & loh suet kee
* date: 6 April 2016
* a class to create activityList object and memberlist object
*/
import java.util.*;
public class FitnessClub {
    public static Scanner sc;
    public static MemberList mb;
    public static ActivityList al;
    public static void main (String [] args)
    {
        sc= new Scanner(System.in);
        mb= new MemberList();
        al=new ActivityList();
        System.out.println("Welcome to Fitness Club!!!");
        
        menu();
    }//end of main method
    
    public static void menu()
    {
      boolean choiceOfelement=false;
      int elementChoice=0;

       do{
          System.out.println("Choose a element you want to modify: ");
          System.out.println("1. MemberList");
          System.out.println("2. ActivityList");
          System.out.println("3. Record Activities for Member");
          System.out.println("4. View all information of Member");
          System.out.println("5. Quit.");
           try{
              elementChoice=sc.nextInt();
              sc.nextLine();
              
              //validate input
              if(elementChoice>=1||elementChoice<=5)
                 choiceOfelement=true;
              else
              {
                 System.out.println("Please enter number [1] to [5].");
                 choiceOfelement=false;
                 sc.next();
              }
            }catch(InputMismatchException e){
               System.out.println("Invalid type. Please enter [1] to [5]. ");
               sc.next();
            }
           
            switch(elementChoice)
            {
               case 1: memberInfo();break;
               case 2: activityInfo();break;
               case 3: recordMemAct(mb);break;
               case 4: displayAllInfo(mb);break;
               case 5: System.out.println("Thanks for using Fitness Club apps."
                           + " Have a nice day!");break;
               default:System.out.println("Invalid choice.");
                       choiceOfelement=false;
                       break;
            }
        }while(!choiceOfelement||elementChoice!=5);
    }// end of method menu
    
    //***ALL MENU OF ACTIVITY***
    //a method to display the ActivityList menu
    public static void activityInfo()
    {
      boolean invalidChoice=false;
        
       int choice=0;
       do{
           do{
               System.out.println("What do you want?");
               System.out.println("1. Add activity");
               System.out.println("2. View information of activities");
               System.out.println("3. Update information about activity");
               System.out.println("4. Back");
               System.out.println("5. QUIT");
               System.out.println("Please choose a number.");
               try{
                  choice=sc.nextInt();
                  sc.nextLine();
                  
                  //validate input
                  if(choice>=1 || choice<= 5)
                     invalidChoice=true;
                }catch(InputMismatchException e){
                  System.out.println("Invalid type. Please enter a [1] to [5].");
                  sc.next();
                  invalidChoice=false;
                }
            }while (invalidChoice=false);
        
             switch(choice)
             {
                case 1: newActivity(al);break;
                case 2: displayActInfo();break;
                case 3: updateActInfo(al);break;
                case 4: menu();break;
                case 5: System.out.println("Thanks for using Fitness Club apps."
                        + " Have a nice day!");break;
                default: System.out.println("Invalid choice.");break;
             }
        }while(choice!=5);
    }//end of activityInfo method
    
    //method to display activity info
    public static void displayActInfo()
    {
        boolean validateChoice=false;
        int infoChoice=0;
        do{
            System.out.println("Which information you wish to see.");
            System.out.println("1. Information about all Activities");
            System.out.println("2. Average MET");
            System.out.println("3. Highest total cost");
            System.out.println("4. back");
            try{
                infoChoice=sc.nextInt();
                sc.nextLine();

                //validate choice
                if(infoChoice>=1||infoChoice<=4)
                    validateChoice=true;

                switch(infoChoice)
                {
                   case 1: System.out.println(al.getAll()); break;
                   case 2: System.out.println("Average MET of all activities is :"
                           + al.averageMET());break;
                   case 3: System.out.println(al.highestTotalCost().getActivityName()
                           +" has the highest total cost of "
                           + al.highestTotalCost().totalCost());break;
                   case 4:;break;
                   case 5: activityInfo();break;
                   default: System.out.println("Please enter number [1] to [4].");
                }
            }catch(InputMismatchException e)
            {
                System.out.println("Invlaid type. Please enter number [1] to [4].");
                validateChoice=false;
                sc.next();
            }
        }while(infoChoice!=4||!validateChoice);
    } //end of display info method
    
     //method to displau menu of update activity information
    public static void updateActInfo(ActivityList a)
    {
      boolean validateChoice=false;
         int updateActInfo=0;
         do{
                System.out.println("Which information you want to update?");
                System.out.println("1. Duration of activity");
                System.out.println("2. Cost per hour");
                System.out.println("3. MET value");
                System.out.println("4. Back");
                try{
                    updateActInfo=sc.nextInt();
                    sc.nextLine();
                    if(updateActInfo>=1&& updateActInfo<=4)
                      validateChoice=true;

                     switch (updateActInfo) 
                     {
                        case 1:updateDuration(al);break;
                        case 2:updateCost(al);break;
                        case 3:updateMET(al);break;
                        case 4: activityInfo();break;
                        default: System.out.println("Please enter number [1] to [4].");break;
                     }
                   }catch(InputMismatchException e)
                   {
                      System.out.println("Invalid type. Please enter number [1] to [4].");       
                      sc.next();
                   }    
            }while(!validateChoice||updateActInfo!=4);
    }
    
    //***ALL MENU OF MEMBER***
    //a method to display menu of memberList
    public static void memberInfo()
    {
      boolean validateChoice=false;
      int choice=0;
       do{
           System.out.println("What do you want?");
           System.out.println("1. Add member");
           System.out.println("2. View Information of members");
           System.out.println("3. Update information of member");
           System.out.println("4. Back");
           System.out.println("5. Quit.");
           try{
               choice=sc.nextInt();
               sc.nextLine();

               //validate input
               if(choice>=1||choice<=5)
                 validateChoice=true;
             
              switch(choice)
              {
                case 1: newMember();break;
                case 2: displayMemInfo();break;
                case 3: updateMbrInfo(mb);break;
                case 4: menu();break;
                case 5: System.out.println("Thanks for using Fitness Club apps."
                        + " Have a nice day!");break;
                default: System.out.println("Please enter number [1] to [5]. ");break;
              }
              
            }catch(InputMismatchException e)
            {
                System.out.println("Invalid type. Please enter number [1] to [5].");
                validateChoice=false;
                sc.next();
            }
        }while(choice!=5||validateChoice==false);
    }//end of memberInfo method
    
    //method to display info of member
    public static void displayMemInfo()
    {
       boolean validateChoice=false;
       int infoChoice=0;
        do{
            System.out.println("Which information you wish to see.");
            System.out.println("1. Information about all members");
            System.out.println("2. The highest BMI");
            System.out.println("3. Average weight");
            System.out.println("4. Avearge height");
            System.out.println("5. Body category of member");
            System.out.println("6. back");
           try{
                infoChoice=sc.nextInt();
                sc.nextLine();

                //validate choice
                if(infoChoice>=1||infoChoice<=6)
                    validateChoice=true;
               
                switch(infoChoice)
                {
                    case 1: System.out.println(mb.getAll()); break;
                    case 2: showHighestBMI(mb);break;
                    case 3: System.out.println("The average weight of the members is: "
                             + mb.aveWeightMember());break;
                    case 4: System.out.println("The average height of the members is: "
                             + mb.aveHeightMember());break;
                    case 5: showBodyType(mb);;break;
                    case 6: break;
                    default:System.out.println("Invalid choice!");break;
                } 
            }catch(InputMismatchException e)
            {
                System.out.println("Please enter number [1] to [6].");
                validateChoice=false;
                sc.next();
            }
        }while(infoChoice!=6||!validateChoice);
    } //end of display info method
    
    //method to update info of member List
    public static void updateMbrInfo(MemberList m)
    {
       boolean validateChoice=false;
       int updateInfo;
        do{
        
          System.out.println("Which information you want to update?");
          System.out.println("1. height");
          System.out.println("2. weight");
          System.out.println("3. back");
          try{
             updateInfo=sc.nextInt();
             sc.nextLine();
          
             //validate input
             if(updateInfo>=1 && updateInfo<=3)
                validateChoice=true;
             
             switch (updateInfo) 
             {
                case 1: updateHeight(m); break;
                case 2: updateWeight(m); break;
                default: System.out.println("Invalid choice!");break;
             }
            }catch(InputMismatchException e)
            {
              System.out.println("Invalid type. Please enter number [1] to [3].");
              sc.next();
            }
        }while(!validateChoice);
    }
    
    //***METHOD OF ADD ***
    // a method to add new member
    public static void newMember()
    {
      boolean validMember=false;
      String memberName;
      double height=0.0;
      double weight=0;
        
       do{
          System.out.println("Enter name of new member: ");
          memberName=sc.nextLine();
        
          //validate name
          isString(memberName);//check if it is a blankSpace
          if(isString(memberName)==false)
          {
            System.out.println("This field is required. Please enter a name.");
          }
        }while(isString(memberName)==false) ;
        
       do{
          System.out.println("Enter weight(kg): ");
          try{
             weight=sc.nextDouble();
             sc.nextLine();
             
             //validate weight
             validateWeight(weight);
             if(validateWeight(weight)==false)
               System.out.println("Please enter your weight between 0-200kg.");
            }catch(InputMismatchException nfe)
            {
               System.out.println("Invalid type. Please enter value between 0-200kg.");
               sc.next();
            }
       }while(validateWeight(weight)==false);
        
       do{
          System.out.println("Enter height(m): ");
          try{
              height=sc.nextDouble();
              sc.nextLine();
             
              //validate height
              validateHeight(height);
              if(validateHeight(height)==false)
                  System.out.println("Please enter height between 0-2.50 m");
            }catch(InputMismatchException e)
            {
                System.out.println("Invalid type. "
                        + "Please enter value between0-2.50m.");
                sc.next();
            }
       }while(!validateHeight(height));
       Member mb1= new Member(memberName,weight,height);
       if (mb.addMember(mb1))
            System.out.println("Member added successfully.");
       else 
            System.out.println("Member added failed. Member is full.");
        System.out.println("\nMember information: ");
        System.out.printf("BMI: %.2f", mb1.getBMI());
        System.out.println("\nBody type: "+ mb1.bodyType()+ "\n");
    
    }//end of new member method
    
    // a method to add activity to the activity list
    public static void newActivity(ActivityList a)
    {
      boolean validActivity=false;
      String activityName;
        do{
          System.out.println("Please enter a new activity.");
          activityName=sc.nextLine();
        
          //validate the activity name
          isString(activityName);
            if(isString(activityName)==false)
            {
               System.out.println("This field is required. Please enter a activity.");
            }
        }while(isString(activityName)==false) ;

      //ask user to enter MET value
      boolean validMET=false;
      double metValue=0;
       do{
            System.out.println("Please enter MET value for this activity.");
           try{
              metValue=sc.nextDouble();

              //validate the MET 
              validateMet(metValue);
              if(validateMet(metValue)==false)
              {
                System.out.println("Enter MET value between 0-20.");
              }
            }catch(InputMismatchException e)
            {
                System.out.println("This field is require. Please enter MET value.");
                sc.next();
            }
        }while(!validateMet(metValue)) ;
        
    //ask user to enter cost per hour
     boolean validCost=false;
     double cost=0;
       do{
           System.out.println("Please enter cost per hour this activity.");
            try{
              cost=sc.nextDouble();
              
              validateCost(cost);
              //validate the cost
              if (validateCost(cost)==false)
              {
                 System.out.println("Please enter cost within the range Rm[0-30]");
                 validCost=false;
              }
              else
                 validCost=true;
            }catch(InputMismatchException e)
            {
                System.out.println("This field is require. Please enter cost.");
                sc.next();
            }
        }while(!validCost) ;
        Activity act= new Activity(activityName,metValue,0,cost);
        if (a.addActivity(act))
            System.out.println("Activity added");
        else
            System.out.println("Activity not added. Activity is full");
    }//end of add new activity
    
    //***METHOD OF SEARCH***
    public static Member searchMember(MemberList m)
    {
        String wanted;
        do{
            System.out.print("Enter Member name :");
            wanted = sc.nextLine();
            isString(wanted);
            Member foundMb = m.findMember(wanted);
               if (foundMb != null)
               {
                  System.out.println(foundMb.toString());
                  return foundMb;
               }
               else
               {   
                  System.out.println("Member with this name not found.");
                  return null;
               }
        }while(!isString(wanted));
    }
    
    public static Activity searchActivity(ActivityList a)
    {
        System.out.print("Enter activity name :");
        String wanted = sc.nextLine();
        Activity foundAct = a.findActivity(wanted);
        if (foundAct != null)
        {
            System.out.println(foundAct.toString());
            return foundAct;
        }
        else
        {   
            System.out.println("Activity with this name not found.");
            return null;
        }
    }
    
    //METHODS OF UPDATE INFORMATION FOR MEMBER
    public static void updateWeight(MemberList m)
    {
        Member foundMb;
       do{
          foundMb=searchMember(m);
          if(foundMb!=null)
          {
            double weight=0.0;
            double height=0;
       
          do{
             System.out.println("Please enter your new weight in kg.");
             try{
                weight=sc.nextDouble();
                sc.nextLine();
       
                //validate input weight
                validateWeight(weight);
                if(validateWeight(weight )==false)
                  System.out.println("Please enter height between 0-2.50m.");
                else
                {
                    System.out.println("Weight updated successfully");
                    foundMb.setWeight(weight);
                }
                    
                }catch(InputMismatchException e)
                {
                  System.out.println("Invalid type. Please enter value from[0]-[2.50]m");
                  sc.next();
                }
                
            }while(validateWeight(weight)==false);
          }
          
       }while(foundMb==null);
    }//end of update weight method
        
    public static void updateHeight(MemberList m)
    {
       Member foundMb;
       do{
          foundMb=searchMember(m);
          double weight=0;
          double height=0;
           if(foundMb!=null)
           {
             do{
                System.out.println("Enter your new height(m): ");
                 try{
                   height=sc.nextDouble();
                   sc.nextLine();

                  // validate height
                  validateHeight(height);
                  if( validateHeight(height )==false)
                     System.out.println("Please enter height between 0-2.50m.");
                  else 
                  { 
                     System.out.println("Height updated successfully.");
                     foundMb.setHeight(height);
                  }
                  }catch(InputMismatchException e)
                  {
                    System.out.println("This field is required. Please enter a value.");
                    sc.next();
                  }
               }while( validateHeight(height)==false);
            }
       }while(foundMb==null);  
    }//end of update height method
     
    //METHODS OF DISPLAY INFO OF MEMBER
    public static void showHighestBMI(MemberList m)
    {
        System.out.printf("%s has the highest BMI of %.2f. \n\n"
                ,m.findHighestBMI(0).getName()
                , m.findHighestBMI(0).getBMI());
    }
    
    public static void showBodyType(MemberList m)
    {
        Member foundMb=searchMember(m);
        if(foundMb!=null)
        {
            System.out.println("Body type: "+foundMb.bodyType());
        }
    } 
    
    //METHODS OF UPDATE INFORMATION FOR ACTIVITY
    public static void updateDuration(ActivityList a)
    {
      double duration=0 ;
       Activity foundAct;
       do{
          foundAct=searchActivity(a);
          if(foundAct!=null)
          {
              do{
              System.out.print("Enter durations in hours: ");
              try{
                   duration = sc.nextDouble();
                   sc.nextLine();
                        
                   if(validateTime(duration))
                   {
                     foundAct.setDurationInHours(duration);
                     System.out.println("Duration updated.\n");
                   }
                   else
                     System.out.println("Please enter duration within 24 hrs.");
                   }catch(InputMismatchException e)
                   {
                     System.out.println("Invalid type. "
                          + "Please enter time within 0-24 hrs.");
                     sc.next();
                   }
              }while(!validateTime(duration));
           } 
        }while(!validateTime(duration)||foundAct==null);
    }//end of updateDuration method
    
    public static void updateCost(ActivityList a)
    {
       double cost=0 ;
       Activity foundAct;
       do{
          foundAct=searchActivity(a);
          if(foundAct!=null)
          {
              do{
                 System.out.println("Enter new cost per hour(RM): ");
                 try{
                     cost=sc.nextDouble();
                     sc.nextLine();
                        
                     if(validateCost(cost))
                     {
                       foundAct.setCosPerHours(cost);
                       System.out.println("Cost updated.\n");
                     }
                     else
                       System.out.println("Please enter cost between RM[0-30].");
                    }catch(InputMismatchException e)
                    {
                       System.out.println("Invalid type. "
                         + "Please enter cost between RM[0-30].");
                     sc.next();
                    }
               }while(!validateCost(cost));
          } 
        }while(!validateCost(cost)||foundAct==null);
    }
    
    public static void updateMET(ActivityList a)
    {
      double metValue=0 ;
       Activity foundAct;
       do{
          foundAct=searchActivity(a);
           if(foundAct!=null)
           {
              do{
                  System.out.println("Enter new MET value: ");
                  try{
                     metValue=sc.nextDouble();
                     sc.nextLine();
                        
                     if(validateMet(metValue))
                     {
                       foundAct.setMET(metValue);
                       System.out.println("MET value updated.\n");
                     }
                     else
                       System.out.println("Please enter value between [0-20].");
                    }catch(InputMismatchException e)
                    {
                       System.out.println("Invalid type. "
                         + "Please enter value between [0-20].");
                       sc.next();
                    }
               }while(!validateMet(metValue));
           } 
        }while(!validateMet(metValue)||foundAct==null);
    }
    
    //METHOD OF RECORD ACTIVITIES FOR MEMBER
     public static void recordMemAct(MemberList m)
    {
       boolean anotherAct=false;
       int anotherUser=0;
       int anotherActivity=0;
       int chosen=0;
       do{
          Member foundMb=searchMember(m);
          if(foundMb!=null)
           {
             do{
                 System.out.println(al.getAll());//display all activities available
              
                 System.out.println("Enter the number you wish to add: ");
                 try{
                 int index=sc.nextInt();
                 sc.nextLine();
                    
                 if(al.numOfActivity==foundMb.numOfAct)
                 {
                     System.out.println("You had done all the activity.");
                     isAnotherLoop(2);
                 }
                 else
                     outerloop:{
                    for(int i=0; i< foundMb.numOfAct; i++)
                    {
                      if(al.activity[i]==foundMb.mylist.activity[index-1])
                       {
                         System.out.println("Activity already chosen. "
                              + "Please choose another activity.");
                         isAnotherLoop(2);
                         anotherAct=false;
                         break outerloop;
                       }    
                    }
                       if(foundMb.addRecord(al.activity[index-1]))
                    {
                       System.out.println("Activity added.");
                       anotherAct=true;
                    }
                    else 
                    {
                       System.out.println("Activity failed to add");
                       anotherAct=false;
                    }  
                   
                 }  
               
                if(anotherAct)
                {
                  double duration=0;
                  double maxTime=0;
                  boolean validatetime=false;
                  boolean exceedTime=false;
                  int memIndex=0;
                   do{
                      System.out.println("Enter the duration for this activity(hours): ");
                      try{
                       do{
                          double duration= sc.nextDouble();
                          sc.nextLine();
                          maxTime+=duration;
                    
                          if(validateTime(duration))
                          {
                            if(maxTime>=24)
                            {
                              System.out.println("Exceeded limit Time. "
                                      + "Please enter time within: "
                                      + (maxTime-duration));
                               maxTime-=duration;
                               exceedTime=false;
                            } 
                            else
                            {
                                foundMb.mylist.activity[index-1].setDurationInHours(duration);
                               System.out.println(foundMb.mylist.activity[index-1].getActivityName());
                               System.out.println(foundMb.mylist.activity[index-1]);
                                foundMb.mylist.activity[index-1].setDurationInHours(duration);
                                maxTime=foundMb.totalDuration(duration);
                                validatetime=true;
                                exceedTime=true;
                            }
                           }
                           else
                           {
                             System.out.println("Please enter time within 0-24 hrs.");
                             maxTime-=duration;
                             validatetime=false;
                           }
                        }while(!exceedTime);
                       }catch(InputMismatchException e)
                       {
                          System.out.println("Invalid Type. Please enter hours within 24 hours.");
                          maxTime-=duration;
                          validatetime=false;
                          sc.next();
                       }
                   }while(!validatetime);
                   
                  //display information
                  System.out.println("Information for activiy "+ foundMb.mylist.activity[index-1].getActivityName()+": ");
                  System.out.println("Calorie Burned for this actvity: "+ foundMb.calBurned()+"kcal");
                  System.out.println("Cost of this activities: RM"+ foundMb.mylist.activity[index-1].totalCost());
                   
                   do{
                     System.out.println("Do you want to enter another activity?");
                     System.out.println("1. Yes");
                     System.out.println("2. No");
                     try{
                        anotherActivity=sc.nextInt();
                        sc.nextLine();

                        //validation of user input
                        isOption(anotherActivity);
                        isAnotherLoop(anotherActivity);

                        }catch(InputMismatchException e)
                        {
                            System.out.println("Invalid type. Please enter [1] or [2].");
                            sc.next();
                       }
                   }while(!isOption(anotherActivity));
                }
                 }catch(InputMismatchException e)
                 {
                     System.out.println("Invalid type.Please enter [1] or [2]");
                     sc.next();
                 }
               }while(!isAnotherLoop(anotherActivity));  
                
            }
       
          do{
            System.out.println("Do you want to choose another user?");
            System.out.println("1. Yes");
            System.out.println("2. No");
            try{
            anotherUser=sc.nextInt();
            sc.nextLine();
            
             //validation of user input
             isOption(anotherUser);
             isAnotherLoop(anotherUser);
             if(anotherUser==2)
                 menu();
            }catch (InputMismatchException e)
            {
                System.out.println("Invalid Type. Please enter [1] or [2].");
                sc.next();
            }
          }while(!isOption(anotherUser));
        }while(!isAnotherLoop(anotherUser));
    }//end of record Member activities
    
     
     //METHOD TO DISPLAY ACTIVITY AND MEMBER INFORMATION 
     //FOR A PARTICULAR MEMBER
     public static void displayAllInfo(MemberList m)
    {
       int anotherUser=0;
       do{
          Member foundMb=searchMember(m);
          if(foundMb!=null)
          {
              System.out.println("Name: "+ foundMb.getName());
              System.out.println("Weight: "+ foundMb.getWeight()+"kg");
              System.out.println("Height: "+ foundMb.getHeight()+"m");
              System.out.printf("BMI: %.2f \n", foundMb.getBMI());
              System.out.println("Body type: "+ foundMb.bodyType());
              System.out.println(foundMb.showAct());
              System.out.printf("Total Calorie Burned: %.2fkcal", foundMb.totalCalorieBurned());
              System.out.printf("\nTotal cost of all activities: RM%.2f\n", foundMb.actTotalCost());
          }
          do{
           System.out.println("Do you want to choose another user?");
           System.out.println("1. Yes");
           System.out.println("2. No");
           try{
           anotherUser=sc.nextInt();
           sc.nextLine();
           
            //validation of user input
           isOption(anotherUser);
           isAnotherLoop(anotherUser);
           if(anotherUser==2)
               menu();
           }catch(InputMismatchException e)
           {
               System.out.println("Invalid type. Please enter [1] or [2].");
               sc.next();
           }
          }while(!isOption(anotherUser));
        }while(!isAnotherLoop(anotherUser));
    }
     
    //***ALL METHODS OF VALIDATION***
    //methods for validation
    public static boolean isString(String memberName)
    {
        if (memberName.equals(""))
                return false;
        else return memberName.trim().length()>0;
    }   
    
    public static boolean validateHeight(double height)
    {
        return height>0 && height <=2.50;
    }
    
    public static boolean validateWeight(double weight)
    {
        return weight>0 && weight <= 200;
    }
    
    public static boolean validateMet( double metVal)
    {
        return metVal>0 && metVal<=20;
    }
    
    public static boolean validateCost(double cost)
    {
        return cost>0 && cost<=30;
    }
     
    //validate if user input 1 or 2
    public static boolean isOption(int input)
    {
        return input==1 || input ==2;
    }
    
    public static boolean isAnotherLoop(int input)
    {
        switch (input) {
            case 1://1.yes
                return false;
            case 2://2.No
                return true;
            default: return false;
        }
    }
    
    public static boolean validateTime(double time)
    {
        return (time>=0 && time<= 24); 
    }
    
}